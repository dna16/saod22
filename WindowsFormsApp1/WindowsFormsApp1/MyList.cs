﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class MyList<T>
    {
        int n = 0;
        int ins = 0;
        T[] data;
        public int inse()
        {
            return ins;
        }
        public MyList(int size)
        {
            data = new T[size+1];
            n = size;
        }
        public void Add(T name)
        {
            n++;
            data[n - 1] = name;
        }
        public void Insert(int ind, T name)
        {
            ins = 0;
            for (int i = n - 1; i >= ind; i--)
            {
                data[i + 1] = data[i];
                ins++;
            }
            data[ind] = name;
            n++;
        }
        public void RemoveAt(int ind)
        {
            for (int i = ind; n > i; i++)
                data[i] = data[i + 1];
            n--;
        }
        public T Last()
        {
            return data[n - 1];
        }
        public T First()
        {
            return data[0];
        }
        public void Clear()
        {
            for (int i = 0; n > i; i++)
                data[i] = default(T);
            n = 0;
        }
        public int Size()
        {
            return 0;
        }
        public string ShowList()
        {
            string str = "";
            for (int i = 0; n > i; i++)
            {
                str += Convert.ToString(data[i]) + " ";
            }
            return str;
        }
    }
}
